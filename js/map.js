function initialize() {

}

var mapProp = {
  center:new google.maps.LatLng(36.313980, 59.527047),
  zoom: 15,
  mapTypeId: google.maps.MapTypeId.ROADMAP
};


$(document).ready(function(){
  var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);

  var myCenter=new google.maps.LatLng(36.313980, 59.527047);
  var marker=new google.maps.Marker({
  position:myCenter,
  });

  marker.setMap(map);
});
