$(document).ready(function(){
  $(".item").addClass("hide");
  $(function() {
    $('a[href*=#]:not([href=#])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html,body').animate({
            scrollTop: target.offset().top
          }, 1000);
          return false;
        }
      }
    });
  });

  $(document).scroll(function(){
    var docH = $(document).scrollTop();
    if(docH > $("#main-menu").next().offset().top || docH == 0){
      showItem($("#main-menu-link"));
    }
    else{
      hideItem($("#main-menu-link"));
    }

    $(".item").each(function(inedx, elem){
      var elemH = $(elem).offset().top;
      if(elemH > docH + 20 && elemH < docH + 600){
        showItem(elem);
      }
      else{
        hideItem(elem);
      }
    })
  });

});

function hideItem(elem){
  $(elem).addClass("hide");
}

function showItem(elem){
  $(elem).removeClass("hide");
}
